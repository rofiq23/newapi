export function createCard(data) {
  const cardContainer = document.getElementById('cardsContainer');
  cardContainer.classList.add('row');

  const colCard = document.createElement('div');
  colCard.classList.add('col-sm-4');
  // colCard.classList.add('bg-dark');

  const card = document.createElement('div');
  card.classList.add('card');
  card.style.width='21rem';

  const cardBody = document.createElement('div');
  cardBody.classList.add('card-body');

  const cardTitle = document.createElement('h5');
  cardTitle.classList.add('card-title');
  cardTitle.textContent = data.title;

  const cardText = document.createElement('p');
  cardText.classList.add('card-text');
  cardText.classList.add('overflow-hidden');
  cardText.textContent = data.content;

  const cardBtn = document.createElement('a');
  cardBtn.classList.add('btn');
  cardBtn.classList.add('btn-primary');
  cardBtn.setAttribute('href',data.url);
  cardBtn.innerHTML = 'More';

  const image = document.createElement('img');
  image.classList.add('card-img-top');
  image.src = data.urlToImage;
  image.alt = data.title;

  const cardAuth = document.createElement('p');
  cardAuth.classList.add('card-author')
  cardAuth.innerHTML=`${data.author} - ${data.publishedAt}`

  cardBody.appendChild(cardTitle);
  cardBody.appendChild(cardAuth);
  cardBody.appendChild(cardText);
  cardBody.appendChild(cardBtn);

  card.appendChild(image);
  card.appendChild(cardBody);

  colCard.appendChild(card)

  cardContainer.appendChild(colCard);
}

