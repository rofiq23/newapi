// datas=null;

// Melakukan GET request
async function getNews(key){
  console.log(key)
  const linkHtml = key === null ? 'keyword':key;
  console.log(linkHtml)
  try{
    const response = await fetch(`https://newsapi.org/v2/everything?q=${linkHtml}&apiKey=d2c1069a170440c6b55482e4a6b4c354`);
    // console.log(response)
    if(!response.ok){
      throw new Error('Network response was not ok');
    }
    const data = await response.json();
    // console.log('datajs\n',data)
    return data;
  }
  catch (error){
    console.error('There was a problem with the fetch operation:', error);
    return null;
  }
}
export {getNews};