import {getNews}  from './src/data.js'
import {createCard} from './src/cardModule.js'
import {createBootstrapNavbar} from './src/navBar.js'

// let news = await getNews();
// news = news.articles

// console.log(news[0],news[2])

createBootstrapNavbar();
// for(let i=0;i<news.length;i++){
// //   createCard(news[i].title, news[i].content, news[i].urlToImage, news[i].url);
//   createCard(news[i]);
// }

async function displayNews(a){
    console.log('dari func',a)
    const newsList = document.getElementById('cardsContainer');
    newsList.innerHTML = '';

    const data = await getNews(a);
    if (data) {
        data.articles.forEach(article => {
          const card = createCard(article);
        //   newsList.insertAdjacentHTML('beforeend', card);
        });
    }
}
displayNews(null);
document.getElementById('findBtn').addEventListener('click', function(event) {
    let searchInputValue = document.getElementById('srcTxt').value;
    event.preventDefault()
    console.log(searchInputValue)
    displayNews(searchInputValue)
});

// function searchNews() {
//     const searchInputValue = document.getElementById('srcTxt').value;
//     alert('Melakukan pencarian untuk: ' + searchInputValue);
//     // displayNews(searchInputValue);
// }